using System;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;

namespace Kasay.HtmlToPdf.UnitTest
{
    public class Render_Test
    {
        const String filePath = "Assets/demo.html";
        const String outputPath = "Output/demo.pdf";

        [Fact]
        public void Iron()
        {
            // Create a PDF from an existing HTML using C#
            var Renderer = new IronPdf.HtmlToPdf();
            var PDF = Renderer.RenderHTMLFileAsPdf(filePath);
            PDF.SaveAs("Output/Iron.pdf");
        }

        [Fact]
        public void Open()
        {
            var lola = File.ReadAllText(filePath);

            var pdf = OpenHtmlToPdf.Pdf
                .From(lola)
                .Content();

            //FOr writing to file from a ByteArray
            File.WriteAllBytes("Output/open.pdf", pdf.ToArray()); // Requires System.Linq
        }

        [Fact]
        public void Select()
        {
            var converter = new SelectPdf.HtmlToPdf();
            var lola = File.ReadAllText(filePath);
            var document = converter.ConvertHtmlString(lola);

            document.Save("Output/select.pdf");
            document.Close();
        }
    }
}
