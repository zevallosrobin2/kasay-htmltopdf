﻿using BenchmarkDotNet.Running;
using System;

namespace Kasay.HtmlToPdf.Benchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<Demo1>();
        }
    }
}
