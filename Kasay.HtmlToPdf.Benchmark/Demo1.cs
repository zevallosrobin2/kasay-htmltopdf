﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kasay.HtmlToPdf.Benchmark
{
    //[SimpleJob(1, 5, 20)]
    [SimpleJob(1, 1, 1)]
    //[CoreJob(true)]
    public class Demo1
    {
        [Params(5, 25)]
        public Int32 Interations { get; set; }

        [Benchmark]
        public Int32 RandomNumber()
        {
            Int32 value = 0;

            for (int i = 0; i < Interations; i++)
            {
                value = new Random().Next();
            }

            return value;
        }

        [Benchmark]
        public String Test()
        {
            return "TEst";
        }
    }
}
